<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0">

  <title><?php echo html($site->title()) ?> | <?php echo html($page->title()) ?></title>
  <meta name="description" content="<?php echo html($site->description()) ?>" />

  <?php echo css('assets/css/main.css') ?>

</head>
<body>

<div id="container">
  <div id="sidebar">
    <a class="logo" href="<?php echo url() ?>">
      <img src="<?php echo url('assets/images/logo.png') ?>" alt="<?php echo html($site->title()) ?>" />
    </a>
    <?php snippet('menu') ?>
    <nav class="languages" role="navigation">
      <ul>
        <?php foreach($site->languages() as $language): ?>
        <li<?php e($site->language() == $language, ' class="active"') ?>>
          <a href="<?php echo $page->url($language->code()) ?>">
            <?php echo html($language->name()) ?>
          </a>
        </li>
        <?php endforeach ?>
      </ul>
    </nav>
  </div>

<div id="name">

</div>
<?php


?>
<!--
<?php echo l::get('versturen'); ?>



  <header class="header cf" role="banner">
    <a class="logo" href="<?php echo url() ?>">
      <img src="<?php echo url('assets/images/logo.jpg') ?>" alt="<?php echo html($site->title()) ?>" />
    </a>
    <?php snippet('menu') ?>
  </header>

-->
